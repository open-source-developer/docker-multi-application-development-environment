# Multi Application Docker Development Environment
This repo contains a boilerplate to deploy multiple docker containers to serve multiple web applications during development, many of our projects that 
use this development environment are built using our open source SaaS framework [Reaktr](https://gitlab.com/open-source-developer/reaktr-framework) 
and normally consist of 2 Reaktr PHP applications (frontend, backend) and a VueJS application (CMS)

If you wish to use this environment for multiple projects then you should have a look at our reverse proxy repo that allows you to proxy local domains
to different ports [Docker NGINX Reverse Proxy](https://gitlab.com/open-source-developer/docker-nginx-reverse-proxy)

### Application Files
Any files that you place inside the following directories will be automatically synced to the containers, file changes
are also synced instantly to your containers:

- applications/frontend/www
- applications/backend/www
- applications/cms/www

### Included Containers
- PHP 8 (FPM)
- NGINX Stable
- MySQL 8
- Redis 6

### Environment Configuration
We have included an ENV file in the project that is used to specify the ports that you would like each container to run on. You should
edit this file to suit your port requirements however we have included some defaults.

### Installation
From the root of this project run:
```shell
docker-compose up -d
```

### Connecting To Containers
if you need to connect directly to your containers you can use the below commands:

#### NGINX
```shell
docker exec -it osd-nginx /bin/sh

# Reloading NGINX after VHOST config changes
nginx -s reload
```

#### PHP
```shell
docker exec -it osd-php /bin/sh
```
#### MySQL
```shell
docker exec -it osd-mysql /bin/sh
```
#### Redis
```shell
docker exec -it osd-redis /bin/sh
```

